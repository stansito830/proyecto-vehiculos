## proyecto-vehiculos

Este proyecto se generó con [Angular CLI] (https://github.com/angular/angular-cli) versión 11.2.1.

- Clonar el proyecto mediante el comando **git clone https://gitlab.com/stansito830/proyecto-vehiculos.git**
- Dentro de la carpeta creada en la clonacion ingresar a **proyecto-vehiculos\transporte**  y ejecutar el comando **pm install -g @angular/cli** que se encargara de descargar las dependencias del proyecto.
- Luego de esto se debera ejecutar el comando **ng serve**  que desplegara la aplicacion en la direccion Local **http://localhost:4200/**
- Se mostrara la pagina de inicio con Login y password. Para ingresar como usuario digitar el usuario "user" y password 123 . Para ingresar como Administrador digitar el usuario "admin" y password 123
### Nota
La aplicacion de spring boot debera estar arriba antes de subir la aplicacion front.
