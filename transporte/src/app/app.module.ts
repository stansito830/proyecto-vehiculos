import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { from } from 'rxjs';
import {HttpClientModule} from '@angular/common/http'
import { AppComponent } from './app.component';
import { AdminComponent } from './components/admin/admin.component';
import { UserComponent } from './components/user/user.component';
import { VerConductorComponent } from './components/ver-conductor/ver-conductor.component';
import { NavUserComponent } from './components/nav-user/nav-user.component';
import { FormsModule } from "@angular/forms";
 


import {ReactiveFormsModule} from "@angular/forms";
//RUTAS 
import {AppRoutingModule} from './app.routes';
import { VerVehiculoComponent } from './components/ver-vehiculo/ver-vehiculo.component';
import { VehiculoComponent } from './components/vehiculo/vehiculo.component';
import { EmpresaComponent } from './components/empresa/empresa.component';
import { VerEmpresaComponent } from './components/ver-empresa/ver-empresa.component';
import { LoginComponent } from './components/login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    UserComponent,
    VerConductorComponent,
    VerVehiculoComponent,
    VehiculoComponent,
    EmpresaComponent,
    VerEmpresaComponent,
    LoginComponent,
    NavUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
