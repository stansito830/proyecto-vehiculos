import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { HttpClient } from "@angular/common/http";
@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private httpClient: HttpClient) {
   
   }
  usuariosData: any = [];

  addForm: FormGroup;
  onSubmit(){
    console.log(this.addForm.value);  
    this.httpClient.post('http://localhost:8080/saveCompany',  this.addForm.value).subscribe(data => {
    console.log(data);
  });
  console.log("Esto se ejecutará antes que el console log de arriba");

  }
  ngOnInit(): void {
    this.addForm = this.formBuilder.group({
      docNumber: ['', Validators.required],
      typeDocument: ['', Validators.required],
      fullName: ['', Validators.required],
      adress: ['', Validators.required],
      city: ['', Validators.required],
      departament: ['', Validators.required],
      country: ['', Validators.required],
      phone: ['', Validators.required],
      docNumberRepresent: ['', Validators.required],
      typeDocumentRepresent: ['', Validators.required],
      nameRepresent: ['', Validators.required],
      adressRepresent: ['', Validators.required],
      cityRepresent: ['', Validators.required],
      departamentRepresent: ['', Validators.required],
      countryRepresent: ['', Validators.required],
      phoneRepresent: ['', Validators.required]
    });

  }

}
