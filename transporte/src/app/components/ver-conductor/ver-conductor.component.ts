import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { HttpClient } from "@angular/common/http";
@Component({
  selector: 'app-ver-conductor',
  templateUrl: './ver-conductor.component.html',
  styleUrls: ['./ver-conductor.component.css']
})
export class VerConductorComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private httpClient: HttpClient) { }

  conductor: any = [];
  addForm: FormGroup;
  updateForm: FormGroup;
  encontrado: boolean =false;
  
  ngOnInit(): void {
    this.addForm = this.formBuilder.group({
        id: ['', Validators.required],
        typeDocument: ['', Validators.required],
        fullName: ['', Validators.required],
        adress: ['', Validators.required],
        city: ['', Validators.required],
        departament: ['', Validators.required],
        country: ['', Validators.required],
        phone: ['', Validators.required]    
    });
  }
  onSubmit(){
    this.encontrado = false;
    console.log(this.addForm.value.id);  
    this.httpClient.get('http://localhost:8080/getDriver/'+this.addForm.value.id).subscribe(data => {
    this.conductor = data[0];

    this.updateForm = this.formBuilder.group(
      this.conductor  
  );

    this.encontrado = true;
    console.log(data);
  }
    , function (error) {
      this.errorSearch = false;
      console.log(error);
    }
  ); 
  }
  onSubmitUpdate(){
    console.log(this.updateForm.value);  
    this.httpClient.post('http://localhost:8080/saveDriver',  this.updateForm.value).subscribe(data => {
    console.log(data);
  });
  } 
}
