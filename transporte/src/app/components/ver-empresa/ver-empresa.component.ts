import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-ver-empresa',
  templateUrl: './ver-empresa.component.html',
  styleUrls: ['./ver-empresa.component.css']
})
export class VerEmpresaComponent implements OnInit {

  
  constructor(private formBuilder: FormBuilder,private httpClient: HttpClient) { }

  conductor: any = [];
  addForm: FormGroup;
  updateForm: FormGroup;
  encontrado: boolean =false;
  ngOnInit(): void {
    this.addForm = this.formBuilder.group({
      docNumber: ['', Validators.required],
      typeDocument: ['', Validators.required],
      fullName: ['', Validators.required],
      adress: ['', Validators.required],
      city: ['', Validators.required],
      departament: ['', Validators.required],
      country: ['', Validators.required],
      phone: ['', Validators.required],
      docNumberRepresent: ['', Validators.required],
      typeDocumentRepresent: ['', Validators.required],
      nameRepresent: ['', Validators.required],
      adressRepresent: ['', Validators.required],
      cityRepresent: ['', Validators.required],
      departamentRepresent: ['', Validators.required],
      countryRepresent: ['', Validators.required],
      phoneRepresent: ['', Validators.required]
    });
  }
  onSubmit(){
    this.encontrado = false;
    console.log(this.addForm.value.docNumber);  
    this.httpClient.get('http://localhost:8080/getCompany/'+this.addForm.value.docNumber).subscribe(data => {
    this.conductor = data[0];

    this.updateForm = this.formBuilder.group(
      this.conductor  
  );

    this.encontrado = true;
    console.log(data);
  }
    , function (error) {
      this.errorSearch = false;
      console.log(error);
    }
  ); 
  }
  onSubmitUpdate(){
    console.log(this.updateForm.value);  
    this.httpClient.post('http://localhost:8080/saveCompany',  this.updateForm.value).subscribe(data => {
    console.log(data);
  });
  } 
}
