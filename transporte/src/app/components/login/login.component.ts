import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router,private formBuilder: FormBuilder) {  }
  addForm: FormGroup;
  ngOnInit(): void {
    this.addForm = this.formBuilder.group({
      user: ['', Validators.required],
      password: ['', Validators.required],
    });
  }
  onLogin(){
   
    console.log(this.addForm.value);

    if (this.addForm.value.user == "admin"){
      this.router.navigate(['admin']);
    }
    
    if (this.addForm.value.user == "user"){
      this.router.navigate(['navUser']);
    }
          
     
 
        

    
  }
}
