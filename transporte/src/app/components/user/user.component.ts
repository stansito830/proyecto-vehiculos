import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { HttpClient } from "@angular/common/http";

import {Router} from "@angular/router";
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private router: Router,private httpClient: HttpClient) {
    this.httpClient.get('http://localhost:8080/getDriver');
   }
   seleccionado:string;
  usuariosData: any = [];
  lista:string[]=[];
  listaData = [{ id: ''}];
  addForm: FormGroup;
  onSubmit(){
   
    //this.addForm.value.carPlate="";
    this.addForm.value.vehicle.id= this.addForm.value.carPlate;
    console.log(this.addForm.value);  
    this.httpClient.post('http://localhost:8080/saveDriver',  this.addForm.value).subscribe(data => {
    console.log(data);
  });
  console.log("Esto se ejecutará antes que el console log de arriba");

  }
  ngOnInit(): void {
    this.addForm = this.formBuilder.group({
      id: ['', Validators.required],
      typeDocument: ['', Validators.required],
      fullName: ['', Validators.required],
      adress: ['', Validators.required],
      city: ['', Validators.required],
      departament: ['', Validators.required],
      country: ['', Validators.required],
      phone: ['', Validators.required],
      vehicle: { id: ''},
      carPlate: ['', Validators.required]
    });
    
    
    this.httpClient.get('http://localhost:8080/getVehicle').subscribe(data => {
      console.log(Object.values(data));
      this.listaData = Object.values(data);
      for (var i in  this.listaData) {

        this.lista.push(this.listaData[i].id)
     }
  });
  }

}
