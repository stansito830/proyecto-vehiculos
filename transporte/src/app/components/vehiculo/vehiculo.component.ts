import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-vehiculo',
  templateUrl: './vehiculo.component.html',
  styleUrls: ['./vehiculo.component.css']
})
export class VehiculoComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private httpClient: HttpClient) {

   }
  seleccionado:string;
  usuariosData: any = [];
  lista:string[]=[];
  listaData = [{ docNumber: ''}];

  addForm: FormGroup;
  onSubmit(){
    this.addForm.value.company.docNumber= this.addForm.value.idCompany;
    console.log(this.addForm.value);  
    this.httpClient.post('http://localhost:8080/saveVehicle',  this.addForm.value).subscribe(data => {
    console.log(data);
  });
  console.log("Esto se ejecutará antes que el console log de arriba");

  }
  ngOnInit(): void {
    this.addForm = this.formBuilder.group({
      id: ['', Validators.required],
      motor: ['', Validators.required],
      chassis: ['', Validators.required],
      model: ['', Validators.required],
      creationDate: ['', Validators.required],
      seatedPassengers: ['', Validators.required],
      standingPassengers: ['', Validators.required],
      dryWeight: ['', Validators.required],
      grossWeight: ['', Validators.required],
      numberOfDoors: ['', Validators.required],
      brand: ['', Validators.required],
      line: ['', Validators.required],
      company: {docNumber: ''},
      idCompany: ['', Validators.required]

    });
    this.httpClient.get('http://localhost:8080/getCompany').subscribe(data => {
      console.log(Object.values(data));
      this.listaData = Object.values(data);
      for (var i in  this.listaData) {

        this.lista.push(this.listaData[i].docNumber)
     }
  });
  }
}
