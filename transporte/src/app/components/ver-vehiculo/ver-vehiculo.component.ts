import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { HttpClient } from "@angular/common/http";
@Component({
  selector: 'app-ver-vehiculo',
  templateUrl: './ver-vehiculo.component.html',
  styleUrls: ['./ver-vehiculo.component.css']
})
export class VerVehiculoComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,private httpClient: HttpClient) { }

  vehiculo: any = [];
  addForm: FormGroup;
  updateForm: FormGroup;
  encontrado: boolean =false;
  ngOnInit(): void {
    this.addForm = this.formBuilder.group({
        id: ['', Validators.required],
        motor: ['', Validators.required],
        chassis: ['', Validators.required],
        model: ['', Validators.required],
        creationDate: ['', Validators.required],
        seatedPassengers: ['', Validators.required],
        standingPassengers: ['', Validators.required],
        dryWeight: ['', Validators.required],
        grossWeight: ['', Validators.required],
        numberOfDoors: ['', Validators.required],
        brand: ['', Validators.required],
        line: ['', Validators.required]
    });
  }
  onSubmit(){
    this.encontrado = false;
    console.log(this.addForm.value.id);  
    this.httpClient.get('http://localhost:8080/getVehicle/'+this.addForm.value.id).subscribe(data => {
    this.vehiculo = data;

    this.updateForm = this.formBuilder.group(
      this.vehiculo  
  );

    this.encontrado = true;
    console.log(data);
  }
    , function (error) {
      this.errorSearch = false;
      console.log(error);
    }
  ); 
  }
  onSubmitUpdate(){
    console.log(this.updateForm.value);  
    this.httpClient.post('http://localhost:8080/saveVehicle',  this.updateForm.value).subscribe(data => {
    console.log(data);
  });
  } 

}
