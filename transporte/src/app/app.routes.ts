import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminComponent} from "./components/admin/admin.component";
import {UserComponent} from "./components/user/user.component";
import {VerConductorComponent} from "./components/ver-conductor/ver-conductor.component";
import {VerVehiculoComponent} from "./components/ver-vehiculo/ver-vehiculo.component";
import {VehiculoComponent} from "./components/vehiculo/vehiculo.component";
import {EmpresaComponent} from "./components/empresa/empresa.component";
import {VerEmpresaComponent} from "./components/ver-empresa/ver-empresa.component";
import {LoginComponent} from "./components/login/login.component";
import {NavUserComponent} from "./components/nav-user/nav-user.component";
const routes: Routes = [

    {path :'admin',component :AdminComponent,
    children: 
    [ 
      {path :'empresa',component :EmpresaComponent},
      {path :'verEmpresa',component :VerEmpresaComponent},
     ]},
    {path :'login',component :LoginComponent},
    {path:'navUser', component: NavUserComponent,
    children: 
    [ 
      {path :'vehiculo',component :VehiculoComponent},
       {path :'verVehiculo',component :VerVehiculoComponent},
       {path :'user',component :UserComponent},
       {path :'verConductor',component :VerConductorComponent},
     ]},
    {path : '**' ,pathMatch: 'full',redirectTo:'login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],

  exports: [RouterModule]
})

//export const app_routing = RouterModule.forRoot(routes);
export class AppRoutingModule { }